describe('First exercise - Reduce', () => {
  describe('forEach', () => {
    let exampleValues;

    beforeEach(() => {
      exampleValues = [
        {a: 1},
        {a: 2},
        {a: 3}
      ];
    });

    it('should call function three times', () => {
      let iterationsCount = 0;
      const countCalls = () => {
        iterationsCount++;
      };
      forEach(exampleValues, countCalls);
      expect(iterationsCount).to.equal(3);
    });

    it('should transform each value of input', () => {
      const incrementParams = value => {
        value.a++;
      };
      forEach(exampleValues, incrementParams);
      const expectedValue = [{a: 2}, {a: 3}, {a: 4}];
      expect(expectedValue).to.deep.equal(expectedValue);
    });

    it('should sum values of input', () => {
      let result = 0;
      const sumParams = value => {
        result += value.a;
      };
      forEach(exampleValues, sumParams);
      expect(result).to.equal(6);
    });

    it('should return undefined', () => {
      const result = forEach(exampleValues, _.noop);
      expect(result).to.be.an('undefined');
    });
  });

  describe('filter', () => {
    let exampleValues;

    beforeEach(() => {
      exampleValues = [
        {a: 1},
        {a: 2},
        {a: 3}
      ];
    });

    it('should call function three times', () => {
      let iterationsCount = 0;
      const countCalls = () => {
        iterationsCount++;
      };
      filter(exampleValues, countCalls);
      expect(iterationsCount).to.equal(3);
    });

    it('should return only greater than 1', () => {
      const filterValues = value => {
        return value.a > 1;
      };
      const result = filter(exampleValues, filterValues);
      const expectedValue = [{a: 2}, {a: 3}];
      expect(result).to.deep.equal(expectedValue);
    });

    it('should return nothing', () => {
      const filterValues = () => {
        return false;
      };
      const result = filter(exampleValues, filterValues);
      const expectedValue = [];
      expect(result).to.deep.equal(expectedValue);
    });

    it('should return all items', () => {
      const filterValues = () => {
        return true;
      };
      const result = filter(exampleValues, filterValues);
      const expectedValue = [
        {a: 1},
        {a: 2},
        {a: 3}
      ];
      expect(result).to.deep.equal(expectedValue);
    });
  });

  describe('reduceRight', () => {
    let exampleValues;

    beforeEach(() => {
      exampleValues = [[0, 1], [2, 3], [4, 5]];
    });

    it('should call function three times', () => {
      let iterationsCount = 0;
      const countCalls = () => {
        iterationsCount++;
      };
      reduceRight(exampleValues, countCalls, []);
      expect(iterationsCount).to.equal(3);
    });

    it('should return reversed array', () => {
      const flattenedValues = (flattened, other) => {
        flattened.push(other);
        return flattened;
      };
      const result = reduceRight(exampleValues, flattenedValues, []);
      const expectedValue = [[4, 5], [2, 3], [0, 1]];
      expect(result).to.deep.equal(expectedValue);
    });

    it('should return array containing 6 elements', () => {
      const flattenedValues = (flattened, other) => {
        return flattened.concat(other);
      };
      const result = reduceRight(exampleValues, flattenedValues, []);
      const expectedValue = [4, 5, 2, 3, 0, 1];
      expect(result).to.deep.equal(expectedValue);
    });
  });

  describe('some', () => {
    describe('when value is bigger than', () => {
      let isBiggerThan10;

      beforeEach(() => {
        isBiggerThan10 = element => {
          return element > 10;
        };
      });

      it('should return false', () => {
        const result = some([2, 5, 8, 1, 4], isBiggerThan10);
        expect(result).to.equal(false);
      });

      it('should return true', () => {
        const result = some([12, 5, 8, 1, 4], isBiggerThan10);
        expect(result).to.equal(true);
      });
    });

    describe('when checking whether a value exists in an array', () => {
      let exampleFruits;

      beforeEach(() => {
        exampleFruits = ['apple', 'banana', 'mango', 'guava'];
      });

      it('should return false when there is a kela', () => {
        const isKelaFruit = value => {
          return value === 'kela';
        };
        const result = some(exampleFruits, isKelaFruit);
        expect(result).to.equal(false);
      });

      it('should return true when there is a banana', () => {
        const isBananaFruit = value => {
          return value === 'banana';
        };
        const result = some(exampleFruits, isBananaFruit);
        expect(result).to.equal(true);
      });
    });

    describe('when executing some functions', () => {
      let sum, firstFunction, secondFunction, thirdFunction, exampleListOfFunctions;

      beforeEach(() => {
        sum = 0;
        firstFunction = () => {
          sum += 1;
          return 1;
        };
        secondFunction = () => {
          sum += 2;
          return 2;
        };
        thirdFunction = () => {
          sum += 3;
          return 3;
        };
        exampleListOfFunctions = [firstFunction, secondFunction, thirdFunction];
      });

      it('should call only some functions of checked array', () => {
        const isBiggerThan1 = fn => {
          return fn() > 1;
        };
        some(exampleListOfFunctions, isBiggerThan1);
        expect(sum).to.equal(3);
      });

      it('should call only first functions of checked array', () => {
        const getTruth = fn => {
          return fn() || true;
        };
        some(exampleListOfFunctions, getTruth);
        expect(sum).to.equal(1);
      });

      it('should call all functions of checked array', () => {
        const getFalse = fn => {
          return fn() && false;
        };
        some(exampleListOfFunctions, getFalse);
        expect(sum).to.equal(6);
      });
    });
  });
});

describe('Second exercise - Currying', () => {
  describe('split', () => {
    const exampleString = "Very long text for apptension test";

    it('should return the same number of keys in the object as the number of words in string', () => {
      const result = words(exampleString);
      expect(result.length).to.equal(6);
    });

    it('should return a true value for example keys in example string', () => {
      const result = words(exampleString);
      expect(result[4]).to.equal('apptension');
    });

    it('should return a two words for one whitespace', () => {
      const result = words(' ');
      expect(result.length).to.equal(2);
    });
  });

  describe('add', () => {
    const exampleArray = [2, 3, 4];

    it('should return the same number of elements in return array', () => {
      const result = mapAdd(exampleArray);
      expect(result.length).to.equal(exampleArray.length);
    });

    it('should return true sum for example array ', () => {
      expect(_.sum(mapAdd(exampleArray))).to.equal(12);
    });
  });

  describe('less', () => {
    const exampleArray = [2, -3, 4, 1];

    it('should return the smallest value for example array', () => {
      const result = lessCheck(exampleArray);
      expect(result).to.equal(-3);
    });
  });
});

describe('Third exercise - Recursion', () => {
  describe('flatMapDeep', () => {
    it('should flaten example from lodash', () => {
      const duplicate = n => {
        return [[[n, n]]];
      };
      const inputArray = _.map([1, 2], duplicate);
      const result = flatMapDeep(inputArray);
      expect(result).to.deep.equal([1, 1, 2, 2]);
    });

    it('should flaten empty array in array in array in array', () => {
      const result = flatMapDeep([[[[[]]]]]);
      expect(result).to.deep.equal([]);
    });

    it('should return the same output', () => {
      const result = flatMapDeep([1, 2, 3]);
      expect(result).to.deep.equal([1, 2, 3]);
    });
  });

  describe('forEach', () => {
    let calls, callback;

    beforeEach(() => {
      calls = 0;
      callback = () => {
        calls++;
      };
    });

    it('should iterate simple array', () => {
      recursiveForEach([1, 2, 3], callback);
      expect(calls).to.eq(3);
    });

    it('should iterate multidimensional array', () => {
      recursiveForEach([1, [2, [3, 4], 5], 5], callback);
      expect(calls).to.eq(6);
    });
  });
});
