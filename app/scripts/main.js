// ======== 1. ========
// -------- forEach --------

function forEach(input, callback) {
  return _.reduce(input, (result, value, key) => {
    callback(value, key, input);
  }, []);
}

// -------- filter --------

function filter(input, callback) {
  return _.reduce(input, (result, value, key) => {
    if (callback(value, key)) {
      result.push(value);
    }
    return result;
  }, []);
}

// -------- reduceRight --------

function reduceRight(input, callback, result) {
  return _.reduce(input.reverse(), callback, result);
}

// -------- some --------

function some(input, iteratee) {
  return _.reduce(input, (result, value, key) => {
    return result || iteratee(value, key, input);
  }, false);
}

// ======== 2. ========
// -------- split --------

var words = _.curry(_.split)(_, ' ', undefined);

// -------- add --------

function add(a, b) {
  return a + b;
}
var mapAdd = _.curry(_.map)(_, _.curry(add)(_, 1));

// -------- less --------

function less(a, b) {
  return a < b ? a : b;
}
var lessCheck = _.curry(_.reduce)(_, less, Infinity);

// ======== 3. ========
// -------- flatMapDeep --------

let flatMapDeep = (arr) => {
  return Array.isArray(arr) ? [].concat.apply([], arr.map(flatMapDeep)) : arr;
};

// -------- forEach --------

let recursiveForEach = (arr, cb) => {
  if (Array.isArray(arr)) {
    for (let k in arr) {
      recursiveForEach(arr[k], cb);
    }
    return;
  }
  cb(arr);
};
