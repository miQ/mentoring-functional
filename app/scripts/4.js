const tracksUrl = 'http://musicbrainz.org/ws/2/release/a4864e94-6d75-4ade-bc93-0dabf3521453?fmt=json&inc=recordings+release-groups+artists';
const albumsUrl = 'http://musicbrainz.org/ws/2/artist/83d91898-7763-47d7-b03b-b92132375c47?fmt=json&inc=release-groups';
let releaseAverageSongLength = 0;
$.getJSON(tracksUrl, (data) => {
  _.forEach(data.media, (media) => {
    media.averageTracksLength = _.sumBy(media.tracks, (track) => track.length)/media.tracks.length;
    releaseAverageSongLength += media.averageTracksLength;
  });
  releaseAverageSongLength /= data.media.length;

  const radius = 100;
  const border = 10;
  const padding = 30;
  const startValue = 0;
  const endValue = releaseAverageSongLength/100;
  const twoPi = Math.PI * 2;
  const boxSize = (radius + padding) * 2;
  const step = 10;

  let arc = d3.arc()
    .startAngle(0)
    .innerRadius(radius)
    .outerRadius(radius - border);

  let parent = d3.select('div.average');

  let svg = parent.append('svg')
    .attr('width', boxSize)
    .attr('height', boxSize);


  let g = svg.append('g')
    .attr('transform', 'translate(' + boxSize / 2 + ',' + boxSize / 2 + ')');

  let meter = g.append('g');

  meter.append('path')
    .attr('fill', '#ccc')
    .attr('fill-opacity', 0.5)
    .attr('d', arc.endAngle(twoPi));


  let front = meter.append('path')
    .attr('fill', 'white')
    .attr('fill-opacity', 1);

  let numberText = meter.append('text')
    .attr('fill', '#fff')
    .attr('text-anchor', 'middle')
    .attr('dy', '.35em');

  function updateProgress(progress) {
    front.attr('d', arc.endAngle(twoPi * progress/1000));
    numberText.text(progress);
  }

  let progress = startValue;

  (function loops() {
    updateProgress(progress);

    if (progress < endValue) {
      progress += step;
      setTimeout(loops, 10);
    }
  })();
});

$.getJSON(albumsUrl, (data) => {
  console.log(data);
  let result = _.filter(data['release-groups'], (group) => !_.some(group['secondary-types'], (type) => type.toLowerCase() === 'compilation'));
  console.log(result);
});


const windowWidth = $(window).width();
let svg = d3.select('.music__bars').append('svg').attr('width', `${windowWidth}px`);
let freqData = new Uint8Array(Math.floor(windowWidth/10));

svg.selectAll('rect')
  .data(freqData)
  .enter()
  .append('rect')
  .attr('height', '100px')
  .attr('width', `${windowWidth/50}px`)
  .attr('fill', 'white')
  .attr('x', (d, i) => i * windowWidth/40);

let audioCtx = new (window.AudioContext || window.webkitAudioContext)();
let audioElement = document.getElementById('audio');
let audioSrc = audioCtx.createMediaElementSource(audioElement);
let analyser = audioCtx.createAnalyser();
audioSrc.connect(analyser);
audioSrc.connect(audioCtx.destination);

const renderChart = () =>  {
  requestAnimationFrame(renderChart);
  analyser.getByteFrequencyData(freqData);
  svg.selectAll('rect')
    .data(freqData)
    .attr('y', function(d) {
      return 200 - d/1.2;
    })
    .attr('height', function(d) {
      return d;
    })
    .attr('fill', 'white');
};

renderChart();